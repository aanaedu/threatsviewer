# ThreatsViewer Django App

This is Django app helps you easily view and track threats from a directory of threat data dumps.

**This app is written with Django 2.1.5 and Python 3. **


## Building

It is best to use the python `virtualenv` tool to build locally:

1. Clone code and start server
    ```sh
    $ git clone https://aanaedu@bitbucket.org/aanaedu/threatsviewer.git
    $ cd threatsviewer
    $ virtualenv env
    $ source env/bin/activate
    $ pip install -r requirements.txt
    ```
2. Install and start redis server
    - On a mac: 
    ``` sh
        $ brew install redis
        $ redis-server /usr/local/etc/redis.conf 
    ```
3. Start server
    ```sh
        $ python manage.py runserver
    ```

4. Run File Watcher

    Open a new terminal or terminal tab to run the file watcher

    If virtualenv (env) is not active, enter the following:

        ```sh
        $ cd threatsviewer
        $ virtualenv env
        $ source env/bin/activate
        $ python file_watcher.py
        ```

    If virtualenv is active, navigate to the project's (threatsviewer) root directory

        ```sh
        $ python file_watcher.py
        ```

5. Visit `http://localhost:8000` to view the app.


## Adding Data files

To add new records, add json files with the signature below:

```
[
    {
        ‘date’: ‘Jan 1, 2015 13:10:59’,
        ‘filename’: ‘virus.exe’,
        ‘action’: ‘files-deleted’,
        ‘submit-type’: ‘FG300B3910602113/root’, 
        ‘rating’: ‘high-risk’
    }, 
    {
        ‘date’: ‘Jan 1, 2015 13:12:59’,
        ‘filename’: ‘helper.exe’,
        ‘action’: ‘files-added’,
        ‘submit-type’: ‘FG300B3910602113/root’, 
        ‘rating’: ‘low-risk’
    } 
]
       
```

Valid rating values are "clean", "low-risk", "medium-risk", "high-risk" and  "malicious".


## Requirements
- channels==2.1.6
- channels-redis==2.3.3
- Django==2.1.5
- watchdog==0.9.0
- redis