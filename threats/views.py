import glob, os, json 

from django.shortcuts import render
from django.http import HttpResponse

from threatsviewer import settings


def index(request):
    return render(request, 'index.html')

def all(self):
    """reads and returns data from json files in a directory"""
    response_data = []
    file_paths = glob.glob(os.path.join(settings.DATA_DIR, '*.json'))
    
    # read each data file from the data directory
    for file_path in file_paths:
        try:
            with open(file_path) as f:
                response_data.extend(json.load(f))
        except:
            print(dumps({ 'message': 'An error occured while reading {}'.format(file_path) }))
    return HttpResponse(json.dumps(response_data), content_type="application/json")
