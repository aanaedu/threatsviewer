$(document).ready(function () {
    const THREATS_COLOR = {
        "clean": "#5acb73",
        "low-risk": "#2196f3",
        "medium-risk": "#f7f702",
        "high-risk": "#f6ba04",
        "malicious": "#db1415"
    };
    const timePeriodSelector = '#js-time-period';
    const addSearchExtension = () => {
        const numberDaysBetween = (date1, date2) => {
            // returns the number of days between two dates
            const difference = Math.abs(date1.getTime() - date2.getTime());
            return difference / (1000 * 60 * 60 * 24);
        };
        
        // Add Search Extension to Datatable
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                const timePeriod = $(timePeriodSelector).val() || '';
                const threatDate = new Date(data[0]); // use data for the date column
                const dateDifference = numberDaysBetween(new Date(), threatDate);
                switch(timePeriod) {
                    case '':
                        return true;
                    case '24-hour':
                        return dateDifference <= 1
                    case  '7-days':
                        return dateDifference <= 7
                    case '4-weeks':
                        return dateDifference <= 28
                    default:
                        return false;
                }
            }
        );
    }

    // Setup
    const dataTable = $('#js-threats-table').DataTable({
        "ajax": {
            "url": "/threats",
            "dataSrc": ""
        },
        "columns": [{
                "data": "date",
                "type": "date"
            },
            {
                "data": "filename"
            },
            {
                "data": "action"
            },
            {
                "data": "submit-type"
            },
            {
                "data": "rating"
            },
        ],
        "order": [
            [0, "asc"]
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            const rating = (aData && aData.rating) ? aData.rating : null; 
            if (rating) {
                $('td', nRow).css('background-color', THREATS_COLOR[rating]);
            }
        }
    });

    const initTimePeriodChangeEventListener = () => {
        $(timePeriodSelector).on('change', () => {
            if (dataTable) {
                dataTable.draw();
            }
        });
    }

    addSearchExtension();
    initTimePeriodChangeEventListener();
});