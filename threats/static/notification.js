window.onload = function() {
  const socket = new WebSocket(`ws://${window.location.host}/ws/events/`);
    socket.onopen = function open() {
      toastr.success('WebSocket', 'Connection created');
    };

    socket.onerror = function(e) {
      console.log('WebSocket connection error.');
      toastr.error('WebSocket', 'Connection error');
    }

    socket.onmessage = function(e) {
      const data = JSON.parse(e.data);
      const text = data['text'];
      toastr.info('File Change Event', text);
      document.getElementById('js-messages-wrapper').innerHTML = `
      <div class="alert alert-primary" role="alert">
        New changes received. <a onclick="window.location.reload()" href="">Click here</a> to reload.
      </div>
      `;
    }

    socket.onclose = function(e) {
      toastr.info('WebSocket', 'Connection closed');
    };

    if (socket.readyState == WebSocket.OPEN) {
      socket.onopen();
    }
}
  