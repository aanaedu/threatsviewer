from django.conf.urls import url
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from notifications.consumers import EventConsumer


websocket_urlpatterns = [
    url(r'^ws/events/', EventConsumer),
]

application = ProtocolTypeRouter({
    # (http->django views is added by default)
     'websocket': AuthMiddlewareStack(
        URLRouter(websocket_urlpatterns)
    ),
})

