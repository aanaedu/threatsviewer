import json

from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


class EventConsumer(WebsocketConsumer):
    def connect(self):
        self.event_group_name = "events"
        async_to_sync(self.channel_layer.group_add)(
            self.event_group_name, 
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.event_group_name, 
            self.channel_name
        )
        self.close()

    def receive(self, text_data):
        async_to_sync(self.channel_layer.group_send)(
            self.event_group_name,
            {
                "type": "event.text",
                "text": text_data
            }
        )
    
    def event_text(self, event):
        # event handler for 'event.text' type
        self.send(text_data=json.dumps({
            "type": "event.text",
            "text": event['text']
        }))