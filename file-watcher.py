import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from threatsviewer import settings
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


class Watcher:
    DIRECTORY_TO_WATCH = settings.DATA_DIR

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Error")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def send_text(text):
        """sends events to websocket group"""
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)('events', {'type': 'event.text', 'text': text})

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("Received created event - {}.".format(event.src_path))
            if (event.src_path.endswith('.json')):
                Handler.send_text(event.event_type)

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            print("Received modified event - {}.".format(event.src_path))
            if (event.src_path.endswith('.json')):
                Handler.send_text(event.event_type)
        
        
        elif event.event_type == 'deleted':
            # Taken any action here when a file is deleted.
            print("Received deleted event - {}.".format(event.src_path))
            Handler.send_text(event.event_type)


if __name__ == '__main__':
    w = Watcher()
    w.run()